import os
from pathlib import Path
import secrets
import pandas as pd

DATASETS_FOLDER = "datasets"


def save_dataset(file=None):

    try:
        if file is None:
            return None
        token = secrets.token_hex(16)
        filename = str(token)+".csv"
        file.save(os.path.join(DATASETS_FOLDER, filename))
        return token
    except:
        return None


def save_dataset_without_outliers(pandas_file=None, token=None):

    try:
        if pandas_file is None:
            return None
        if token is None:
            return None
        filename = str(token)+"_outliers.csv"
        pandas_file.to_csv(os.path.join(DATASETS_FOLDER, filename), index=False)
        return token
    except:
        return None



def get_dataset(token=None):

    try:
        if token is None:
            return None

        filename = str(token)+"_outliers.csv"
        if os.path.isfile(os.path.join(DATASETS_FOLDER, filename)):
            return pd.read_csv(os.path.join(DATASETS_FOLDER, filename))
        else:
            filename = str(token) + ".csv"
            if os.path.isfile(os.path.join(DATASETS_FOLDER, filename)):
                return pd.read_csv(os.path.join(DATASETS_FOLDER, filename))
            else:
                return None
    except:
        return None