import fs_regression_algorithms as regression
import fs_classification_algorithms as classification




"""Ejecutar todos los algoritmos de fs_regression_algorithms y meter los resultados en una lista de json con claves {r2_score, mse, mae, number_of_original_features, number_of_selected_features, selected_features}. Devolver la lista"""
def execute_regression_algorithms(inputs=None, outputs=None):
    stats = []

    #43 methods
    stats.append(regression.RFE_ridge_regression(inputs,outputs))
    stats.append(regression.RFE_lasso_regression(inputs,outputs))
    stats.append(regression.RFECV_ridge_regression(inputs,outputs))
    stats.append(regression.genericUnivariateSelect_f_regression_fdr(inputs,outputs))
    stats.append(regression.genericUnivariateSelect_f_regression_fpr(inputs,outputs))
    stats.append(regression.genericUnivariateSelect_f_regression_fwe(inputs,outputs))
    #stats.append(regression.genericUnivariateSelect_f_regression_k_best_middle(inputs,outputs))
    stats.append(regression.genericUnivariateSelect_f_regression_percentile_50(inputs,outputs))
    stats.append(regression.genericUnivariateSelect_mutual_info_regression_fdr(inputs,outputs))
    stats.append(regression.genericUnivariateSelect_mutual_info_regression_fpr(inputs,outputs))
    stats.append(regression.genericUnivariateSelect_mutual_info_regression_fwe(inputs,outputs))
    #stats.append(regression.genericUnivariateSelect_mutual_info_regression_k_best_middle(inputs,outputs))
    stats.append(regression.genericUnivariateSelect_mutual_info_regression_percentile_50(inputs,outputs))
    stats.append(regression.RFE_extra_trees_regression(inputs,outputs))
    stats.append(regression.RFE_linear_regression(inputs,outputs))
    stats.append(regression.RFE_linear_svr_regression(inputs,outputs))
    stats.append(regression.RFECV_extra_trees_regression(inputs,outputs))
    stats.append(regression.RFECV_lasso_regression(inputs,outputs))
    stats.append(regression.RFECV_linear_regression(inputs,outputs))
    stats.append(regression.RFECV_linear_svr_regression(inputs,outputs))
    stats.append(regression.SelectFdr_f_regression_alpha_05(inputs,outputs))
    stats.append(regression.SelectFdr_mutual_info_regression_alpha_05(inputs,outputs))
    stats.append(regression.SelectFpr_f_regression_alpha_05(inputs,outputs))
    stats.append(regression.SelectFpr_mutual_info_regression_alpha_05(inputs,outputs))
    stats.append(regression.SelectFromModel_extra_trees_regression(inputs,outputs))
    stats.append(regression.SelectFromModel_lasso_regression(inputs,outputs))
    stats.append(regression.SelectFromModel_linear_regression(inputs,outputs))
    stats.append(regression.SelectFromModel_ridge_regression(inputs,outputs))
    stats.append(regression.SelectFromModel_svr_regression(inputs,outputs))
    stats.append(regression.SelectFwe_f_regression_alpha_05(inputs,outputs))
    stats.append(regression.SelectFwe_mutual_info_regression_alpha_05(inputs,outputs))
    #stats.append(regression.selectKBest_f_regression_percentile_middle(inputs,outputs))
    #stats.append(regression.selectKBest_mutual_info_regression_middle(inputs,outputs))
    #tats.append(regression.selectPercentile_f_regression_percentile_50(inputs,outputs))
    #stats.append(regression.selectPercentile_mutual_info_regression_percentile_50(inputs,outputs))
    stats.append(regression.VarianceThreshold_threshold_0(inputs,outputs))
    stats.append(regression.VarianceThreshold_threshold_05(inputs,outputs))
    stats.append(regression.VarianceThreshold_threshold_08(inputs,outputs))
    stats.append(regression.yellowbrick_RFECV_extra_trees_regression(inputs,outputs))
    stats.append(regression.yellowbrick_RFECV_lasso_regression(inputs,outputs))
    stats.append(regression.yellowbrick_RFECV_linear_regression(inputs,outputs))
    stats.append(regression.yellowbrick_RFECV_linear_svr_regression(inputs,outputs))
    stats.append(regression.yellowbrick_RFECV_ridge_regression(inputs,outputs))

    # using list comprehension
    # to remove None values in list
    stats = [i for i in stats if i]

    print(stats)
    #TODO incluir graphical solutions
    return stats



"""Ejecutar todos los algoritmos de fs_regression_algorithms y meter los resultados en una lista de json con claves {f2_score, accuracy, number_of_original_features, number_of_selected_features, selected_features}. Devolver la lista"""
def execute_classification_algorithms(inputs=None, outputs=None):
    stats = []
    #TODO incluir algoritmos
    return stats


def get_selected_features_string(string_list):
    string_list.sort()
    return "".join(string_list)

def compute_frequencies_of_sets(stats):
    frequencies = {}

    #get frequencies TODO ver si aparecen en distintos ordenes pero siguen siendo las mismas
    for json in stats:
        selected_features = get_selected_features_string(json["selected_features"])
        if selected_features in frequencies:
            frequencies[selected_features] = frequencies[selected_features]+1
        else:
            frequencies[selected_features] = 1

    #annotate frequencies
    results = []
    for json in stats:
        frequency = frequencies[get_selected_features_string(json["selected_features"])]
        json["frequency"] = frequency
        results.append(json)

    return results




def order_solutions_by(stats, criteria=None, reverse=False):
    # TODO ordenar por criterio y en orden
    results = stats
    if isinstance(criteria, list) == False:
            results.sort(key=lambda k: k[criteria], reverse=reverse)
    else:
        if isinstance(reverse, list) == False:
            results.sort(key=lambda k: (k[criteria[0]], k[criteria[1]]), reverse=reverse)
        else:
            results.sort(key=lambda k: (k[criteria[0]] if reverse[0]==False else -k[criteria[0]], k[criteria[1]] if reverse[1]==False else -k[criteria[1]]))
    return results

def format_regression_stats(stats, initial_inputs = None, initial_score=None, initial_mse=None, initial_mae=None):
    results = {}
    results["initial_inputs"] = initial_inputs
    results["initial_score"] = initial_score
    results["initial_mse"] = initial_mse
    results["initial_mae"] = initial_mae

    stats = compute_frequencies_of_sets(stats)

    results["most_frequent_sets"] = order_solutions_by(stats.copy(),["frequency", "r2_score"], True)
    results["best_r2_score_sets"] = order_solutions_by(stats.copy(),"r2_score", True)
    results["minimum_features_sets"] = order_solutions_by(stats.copy(),["number_of_selected_features","r2_score"], False)

    return results

"""TODO ampliar con algoritmos que no ejecutan la seleccion automática, sino que devuelven una gráfica"""
def execute_selection(type=None, inputs=None, outputs=None):
    # Ejecutar los modelos sobre el conjunto inicial, para obtener los valores iniciales con todos los datos
    # Ejecutar los algoritmos
    # Ordenar los resultados en tres listas, según los criterios marcados en papel

    if type == "regression":
        score, mse, mae, _, _, _ = regression.evaluate_subset(inputs, outputs)
        print(inputs.shape)
        print(outputs.shape)
        stats = execute_regression_algorithms(inputs,outputs)
        formated_stats = format_regression_stats(stats, inputs.shape[1], score, mse, mae)
        return formated_stats

    elif type == "classification":

        return {}
    else:
        print("not type valid")
        return None