from flask import Flask, request, jsonify, send_from_directory
from flask_cors import CORS
import json

from file_methods import *
from outliers import *
from feature_selection import execute_selection
from generate_model import generate_model
from dataset_processing_methods import get_description_dataset, transform_colums, drop_na
import requests

app = Flask(__name__)
CORS(app)




#STEP 1 sube el archivo y se le devuelve un token
@app.route('/file',methods = ['POST'])
def save_file():  # put application's code here
    file = request.files['file']

    if file is None:
        return 'File not uploaded', 400

    token = save_dataset(file)

    if token is None:
        return 'Server error', 500

    return jsonify({"token": token}), 201

#STEP 2 obtiene información del dataset y de sus columnas
@app.route('/description/<token>',methods = ['GET'])
def get_description(token):  # put application's code here
    dataset = get_dataset(token=token)

    if dataset is None:
        return 'File not found', 400

    description = get_description_dataset(dataset)

    return jsonify(description), 201



#STEP 3 Recibe las caracteristicas de entrada y las de salida y ejecuta el borrado de outliers. Devuelve el numero de muestras originales y el número de muestras tras el borrado de outliers
@app.route('/outliers',methods = ['POST'])
def delete_outliers():  # put application's code here

    token = request.get_json()['token']
    inputs = request.get_json()['inputs']
    types = request.get_json()['types']

    dataset = get_dataset(token=token)

    if dataset is None:
        return 'File not found', 400


    X = dataset[inputs]
    X = drop_na(X)

    if X is None:
        return '', 404

    print("X")
    print(X.columns)

    X = transform_colums(X, types)

    dataset, statistics = filter_outliers(dataset, X)

    save_dataset_without_outliers(pandas_file=dataset, token=token)

    return jsonify(statistics), 200



#STEP 4. Ejecuta los algoritmos del tipo que se le pasa por parámetro con las características de entrada que se le pasan por parámetro.
# Devuelve las características seleccionadas, las métricas obtenidas en las predicciones con estas características en cada modelo
@app.route('/select',methods = ['POST'])
def select_features():  # put application's code here
    token = request.get_json()['token']
    inputs = request.get_json()['inputs']
    outputs = request.get_json()['outputs']
    types = request.get_json()['types']
    type = request.get_json()['type']

    dataset = get_dataset(token=token)

    if dataset is None:
        return 'File not found', 400

    dataset = dataset[inputs+outputs]
    dataset = drop_na(dataset)

    if dataset is None:
        return '',404

    X = dataset[inputs]
    y = dataset[outputs]

    X = transform_colums(X, types)
    y = transform_colums(y, types)

    statistics = execute_selection(type, X, y)

    return jsonify(statistics), 200

#STEP 5. Recibe el conjunto final de características y el modelo que se quiere usar. Devuelve el dataset (sin outliers si borrados) con las caracterísicas generadas, el modelo entrenado, las cabeceras del modelo y un JSON con las métricas del modelo.
@app.route('/model',methods = ['POST'])
def get_model():  # put application's code here

    token = request.get_json()['token']
    inputs = request.get_json()['inputs']
    outputs = request.get_json()['outputs']
    types = request.get_json()['types']
    type = request.get_json()['type']

    dataset = get_dataset(token=token)

    if dataset is None:
        return 'File not found', 400

    dataset = dataset[inputs + outputs]
    dataset = drop_na(dataset)

    if dataset is None:
        return '', 404

    X = dataset[inputs]
    y = dataset[outputs]

    X = transform_colums(X, types)
    y = transform_colums(y, types)

    filepath, filename = generate_model(type, X, y, token)

    if filepath == None:
        return "Error", 500
    return send_from_directory(filepath, filename)


@app.route('/')
def hello_world():  # put application's code here
    return 'Hello World!'


if __name__ == '__main__':
    app.run(host="0.0.0.0",port=33366)
