import pandas as pd
import json

TYPES = {"categoric": "C",
         "cuantitive": "N"}



def transform_colums(data=None, types=None):

    if data is None:
        return None

    if types is None:
        return None
    columns = data.columns
    for column in columns:
        if column in types:
            type = types[column]
            if type==TYPES["categoric"]:
                data = pd.get_dummies(data, columns=[column], prefix=column)
                print("Transforming",str(column))
                print(data)
                print(data.columns)
    return data

def drop_na(df):
    df = df.dropna()
    if df.shape[0]==0:
        return None
    return df

def get_description_dataset(data=None):

    if data is None:
        return None

    number_of_samples = data.shape[0]
    number_of_features = data.shape[1]

    # 1. get columns data types
    data_types = {}
    for column in data.columns:
        data_types[column] = str(data[column].dtype)
    #print(data_types)

    # 2. get description of all values
    statistics = json.loads(data.describe(include='all').to_json())

    # 3. generate final results JSON

    description = { "number_of_samples": number_of_samples,
                    "number_of_features": number_of_features,
                    "data_types": data_types,
                    "statistics": statistics}

    return description
