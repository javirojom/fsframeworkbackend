
from sklearn.neighbors import LocalOutlierFactor

def filter_outliers(dataset, X, contamination=None):
    if contamination is None:
        clf = LocalOutlierFactor(n_neighbors=2)
    else:
        clf = LocalOutlierFactor(n_neighbors=2, contamination=contamination)
    outliers_index = clf.fit_predict(X)
    #clf.negative_outlier_factor_

    indexes = []
    index = 0
    for value in outliers_index:
        if value==-1:
            indexes.append(index)
        index = index + 1

    dataset_clean = dataset.drop(dataset.index[indexes])

    statistics = {
        "number_of_original_rows": dataset.shape[0],
        "number_of_filtered_rows": dataset_clean.shape[0]
    }

    return dataset_clean, statistics

