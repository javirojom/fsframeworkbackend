
import warnings
from termcolor import colored
warnings.filterwarnings('ignore')

import pandas as pd
import numpy as np
from sklearn.model_selection  import train_test_split
import matplotlib.pyplot as plt
import time
from sklearn.multioutput import MultiOutputRegressor #for multiple output models

#for filter methods
from sklearn.feature_selection import *
from yellowbrick.features import *
from yellowbrick.target import FeatureCorrelation

# for embedded methods
from sklearn.feature_selection import *
from yellowbrick.model_selection import *
from yellowbrick.model_selection.rfecv import RFECV


# def comma_to_point(x):
#     if isinstance(x, str):
#         return float(x.replace(',', '.'))
#     else:
#         return x
#
# data = data.applymap(comma_to_point)
#

# ### Evaluate model function

from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error

def check_r2_score_for_model(model, X_train, y_train, X_test, y_test, y_pred):
    score = r2_score(y_test, y_pred)
    #print("$R^2$ Coefficient of Determination: ", score)
    return score


# ### Evaluate resulting subset in selected models function

# In[15]:


# Scikit-learn prediction compared to hand-made prediction
from sklearn.linear_model     import LinearRegression, Ridge, Lasso
from sklearn.tree import ExtraTreeRegressor
from sklearn.svm import LinearSVR


def get_results_per_model(model, X_train, X_test, y_train, y_test):
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    score = check_r2_score_for_model(model, X_train, y_train, X_test, y_test, y_pred)
    mse = mean_squared_error(y_test, y_pred)
    mae = mean_absolute_error(y_test, y_pred)
    return score, mse, mae

def evaluate_subset(X,y):
    X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.33, random_state=42)
    print(X_train.shape)
    print(X_test.shape)
    print(y_train.shape)
    print(y_test.shape)

    if y.shape[1]>1:
        lr_model = MultiOutputRegressor(LinearRegression())
        ridge_model = MultiOutputRegressor(Ridge())
        lasso_model = MultiOutputRegressor(Lasso())
        extra_model = MultiOutputRegressor(ExtraTreeRegressor())
        svr_model = MultiOutputRegressor(LinearSVR())
    else:
        lr_model = LinearRegression()
        ridge_model = Ridge()
        lasso_model = Lasso()
        extra_model = ExtraTreeRegressor()
        svr_model = LinearSVR()

    lr, lr_mse, lr_mae = get_results_per_model(lr_model,X_train, X_test, y_train, y_test)
    ridge, ridge_mse, ridge_mae = get_results_per_model(ridge_model,X_train, X_test, y_train, y_test)
    lasso, lasso_mse, lasso_mae = get_results_per_model(lasso_model,X_train, X_test, y_train, y_test)
    extra, extra_mse, extra_mae = get_results_per_model(extra_model,X_train, X_test, y_train, y_test)
    svr, svr_mse, svr_mae = get_results_per_model(svr_model,X_train, X_test, y_train, y_test)
    
    lst = [lr,ridge,lasso,extra,svr]
    avg = sum(lst) / len(lst)

    lst_mse = [lr_mse,ridge_mse,lasso_mse,extra_mse,svr_mse]
    avg_mse = sum(lst_mse) / len(lst_mse)

    lst_mae = [lr_mae,ridge_mae,lasso_mae,extra_mae,svr_mae]
    avg_mae = sum(lst_mae) / len(lst_mae)

    print("Number of features", X.shape[1])
    print("Linear Regression r2 score:",lr)
    print("Ridge Regression r2 score:",ridge)
    print("Lasso Regression r2 score:",lasso)
    print("Extra Trees Regression r2 score:",extra)
    print("Linear SVR r2 score:",svr)
    print("Avg r2 score:",avg)
    
    return avg,avg_mse,avg_mae,lst,lst_mse,lst_mae


# ### Auxiliar methods

# In[16]:


def get_resulting_X(X, Xs_selected): #union
    delete = []
    number_of_Xs = len(Xs_selected)
    for index in range(0,X.shape[1]):
        colname =  X.columns[index]
        important = number_of_Xs
        for matrix in Xs_selected:
            if matrix[0][index]==0:
                important = important - 1
        if important == 0:
            delete.append(colname)
    return X.drop(columns=delete)

def get_resulting_X_yellowbrick(X, Xs_selected): #union
    delete = []
    number_of_Xs = len(Xs_selected)
    for index in range(0,X.shape[1]):
        colname =  X.columns[index]
        important = number_of_Xs
        for matrix in Xs_selected:
            if matrix[index]!=1:
                important = important - 1
        if important == 0:
            delete.append(colname)
    return X.drop(columns=delete)































# # <u>FILTER METHODS</u>


def genericUnivariateSelect_f_regression_percentile_50(X, y):
    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = GenericUnivariateSelect(f_regression, mode='percentile', param=50)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### I.II. Score_function=mutual_info_regression and percentile(50)

def genericUnivariateSelect_mutual_info_regression_percentile_50(X, y):

    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = GenericUnivariateSelect(mutual_info_regression, mode='percentile', param=50)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### I.III. Score_function=f_regression and k_best(15)

def genericUnivariateSelect_f_regression_k_best_middle(X, y):

    k =  X.shape[1]/2 if X.shape[1]/2 > 0 else 1
    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = GenericUnivariateSelect(f_regression, mode='k_best', param=k)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### I.IV. Score_function=mutual_info_regression and k_best(15)

def genericUnivariateSelect_mutual_info_regression_k_best_middle(X, y):

    k =  X.shape[1]/2 if X.shape[1]/2 > 0 else 1
    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = GenericUnivariateSelect(mutual_info_regression, mode='k_best', param=k)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### I.V. Score_function=f_regression and fpr

def genericUnivariateSelect_f_regression_fpr(X, y):

    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = GenericUnivariateSelect(f_regression, mode='fpr')
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except:
        return None

# ### I.VI. Score_function=mutual_info_regression and fpr

def genericUnivariateSelect_mutual_info_regression_fpr(X, y):

    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = GenericUnivariateSelect(mutual_info_regression, mode='fpr')
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))
        return None


# ### I.VII. Score_function=f_regression and fdr

def genericUnivariateSelect_f_regression_fdr(X, y):

    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = GenericUnivariateSelect(f_regression, mode='fdr')
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except:
        return None

# ### I.VIII. Score_function=mutual_info_regression and fdr

def genericUnivariateSelect_mutual_info_regression_fdr(X, y):

    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = GenericUnivariateSelect(mutual_info_regression, mode='fdr')
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))


# ### I.IX. Score_function=f_regression and fwe

def genericUnivariateSelect_f_regression_fwe(X, y):

    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = GenericUnivariateSelect(f_regression, mode='fwe')
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except:
        return None

# ### I.X. Score_function=mutual_info_regression and fwe
def genericUnivariateSelect_mutual_info_regression_fwe(X, y):

    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = GenericUnivariateSelect(mutual_info_regression, mode='fwe')
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))


# ## II. SelectPercentile from sklearn.feature_selection

# ### II.I. Score_function=f_regression with percentile equal to 50
def selectPercentile_f_regression_percentile_50(X, y):

    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = SelectPercentile(f_regression,50)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### II.II. Score_function=mutual_info_regression with percentile equal to 50
def selectPercentile_mutual_info_regression_percentile_50(X, y):

    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = SelectPercentile(mutual_info_regression,50)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ## III. SelectKBest from sklearn.feature_selection

# ### III.I. Score_function=f_regression with k equal to 15

def selectKBest_f_regression_percentile_middle(X, y):

    k = X.shape[1] / 2 if X.shape[1] / 2 > 0 else 1
    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = SelectKBest(f_regression,k)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### III.II. Score_function=mutual_info_regression with k equal to 15
def selectKBest_mutual_info_regression_middle(X, y):

    k = X.shape[1] / 2 if X.shape[1] / 2 > 0 else 1
    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = SelectKBest(mutual_info_regression,k)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ## IV. SelectFpr from sklearn.feature_selection

# ### IV.I. Score_function=f_regression with alpha equal to 0.05
def SelectFpr_f_regression_alpha_05(X, y):

    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = SelectFpr(f_regression,alpha=0.05)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}

# ### IV.II. Score_function=mutual_info_regression with alpha equal to 0.05
def SelectFpr_mutual_info_regression_alpha_05(X, y):

    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = SelectFpr(mutual_info_regression,alpha=0.05)
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))
        return None


# ## V. SelectFdr from sklearn.feature_selection

# ### V.I. Score_function=f_regression with alpha equal to 0.05
def SelectFdr_f_regression_alpha_05(X, y):

    print(X.shape)
    print(y.shape)
    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = SelectFdr(f_regression,alpha=0.05)
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print("error")
        print(e)
        return None

# ### V.II. Score_function=mutual_info_regression with alpha equal to 0.05
def SelectFdr_mutual_info_regression_alpha_05(X, y):

    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = SelectFdr(mutual_info_regression,alpha=0.05)
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))
        return None


# ## VI. SelectFwe from sklearn.feature_selection

# ### VI.I. Score_function=f_regression with alpha equal to 0.05
def SelectFwe_f_regression_alpha_05(X, y):


    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = SelectFwe(f_regression,alpha=0.05)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### VI.II. Score_function=mutual_info_regression with alpha equal to 0.05
def SelectFwe_mutual_info_regression_alpha_05(X, y):

    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = SelectFwe(mutual_info_regression,alpha=0.05)
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))
        return None


# ## VII. VarianceThreshold from sklearn.feature_selection

# ### VII.I. Threshold equal to 0
def VarianceThreshold_threshold_0(X, y):

    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = VarianceThreshold(threshold=0)
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))
        return None

# ### VII.II. Threshold equal to 0.5
def VarianceThreshold_threshold_05(X, y):

    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = VarianceThreshold(threshold=0.5)
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))
        return None

# ### VII.III. Threshold equal to 0.8
def VarianceThreshold_threshold_08(X, y):

    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = VarianceThreshold(threshold=0.8)
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))
        return None

# ## VIII. Rank1D from yellowbrick.features

# ### VIII.I. Algorithm "shapiro"

def visual_Rank1D_shapiro(X,y):
    time_init = time.time()
    visualizer = Rank1D(algorithm='shapiro')

    visualizer.fit(X, y)           # Fit the data to the visualizer
    visualizer.transform(X)        # Transform the data
    visualizer.show()              # Finalize and render the figure

    time_end = time.time()
    print("Time to select features:", (time_end-time_init))

# ## IX. Rank2D from yellowbrick.features

# ### IX.I. Algorithm "pearson"
def visual_Rank1D_pearson(X,y):

    time_init = time.time()
    # Instantiate the visualizer with the covariance ranking algorithm
    visualizer = Rank2D(algorithm='pearson')

    visualizer.fit(X, y)           # Fit the data to the visualizer
    visualizer.transform(X)        # Transform the data
    visualizer.show()              # Finalize and render the figure
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))


# ### IX.II. Algorithm "covariance"

def visual_Rank1D_covariance(X,y):

    time_init = time.time()
    # Instantiate the visualizer with the covariance ranking algorithm
    visualizer = Rank2D(algorithm='covariance')

    visualizer.fit(X, y)           # Fit the data to the visualizer
    visualizer.transform(X)        # Transform the data
    visualizer.show()              # Finalize and render the figure
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))


# ### IX.III. Algorithm "spearman"

def visual_Rank1D_spearman(X,y):

    time_init = time.time()
    # Instantiate the visualizer with the covariance ranking algorithm
    visualizer = Rank2D(algorithm='spearman')

    visualizer.fit(X, y)           # Fit the data to the visualizer
    visualizer.transform(X)        # Transform the data
    visualizer.show()              # Finalize and render the figure
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))



# ### IX.IV. Algorithm "kendalltau"
def visual_Rank1D_kendalltau(X,y):

    time_init = time.time()
    # Instantiate the visualizer with the covariance ranking algorithm
    visualizer = Rank2D(algorithm='kendalltau')

    visualizer.fit(X, y)           # Fit the data to the visualizer
    visualizer.transform(X)        # Transform the data
    visualizer.show()              # Finalize and render the figure
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))


# ## X. FeatureCorrelation from yellowbrick.target

# ### X.I. Method "pearson"
def visual_FeatureCorrelation_pearson(X,y):

    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        print(colname)
        visualizer = FeatureCorrelation(method="pearson")
        visualizer.fit(X, y[colname])
        visualizer.show()
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))


# ### X.II. Method "mutual_info-regression"
def visual_FeatureCorrelation_mutual_info(X,y):

    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        visualizer = FeatureCorrelation(method="mutual_info-regression")
        visualizer.fit(X, y[colname])
        visualizer.show()
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))


# <br/><br/><br/><br/><br/><br/><br/><br/><br/>





# # <u>EMBEDDED METHODS</u>

# ## I. SelectFromModel from sklearn.feature_selection

# ### I.I. Wrap Linear Regression
def SelectFromModel_linear_regression(X, y):
    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = SelectFromModel(estimator=LinearRegression())
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### I.II. Wrap Ridge Regression
def SelectFromModel_ridge_regression(X, y):

    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = SelectFromModel(estimator=Ridge())
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### I.III. Wrap Lasso Regression
def SelectFromModel_lasso_regression(X, y):

    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = SelectFromModel(estimator=Lasso())
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### I.IV. Wrap Extra Trees Regression
def SelectFromModel_extra_trees_regression(X, y):

    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = SelectFromModel(estimator=ExtraTreeRegressor())
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### I.V. Wrap Linear SVR Regression
def SelectFromModel_svr_regression(X, y):

    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = SelectFromModel(estimator=LinearSVR())
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ## II. SequentialFeatureSelector from sklearn.feature_selection

# k-fold(5) and n_features_to_select(0.5) for all

# ### II.I. Wrap Linear Regression

# In[ ]:





# ### II.II. Wrap Ridge Regression

# In[ ]:





# ### II.III. Wrap Lasso Regression

# In[ ]:





# ### II.IV. Wrap Extra Trees Regression

# In[ ]:





# ### II.V. Wrap Linear SVR Regression

# In[ ]:





# ## III. RFE from sklearn.feature_selection

# step(1) and n_features_to_select(5) for all

# In[49]:





# ### III.I. Wrap Linear Regression

def RFE_linear_regression(X, y):
    step = 1
    n_features = 5
    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = RFE(estimator=LinearRegression(), n_features_to_select=n_features, step=step)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### III.II. Wrap Ridge Regression
def RFE_ridge_regression(X, y):
    step = 1
    n_features = 5
    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = RFE(estimator=Ridge(), n_features_to_select=n_features, step=step)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None

    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### III.III. Wrap Lasso Regression
def RFE_lasso_regression(X, y):
    step = 1
    n_features = 5
    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = RFE(estimator=Lasso(), n_features_to_select=n_features, step=step)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### III.IV. Wrap Extra Trees Regression
def RFE_extra_trees_regression(X, y):
    step = 1
    n_features = 5
    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = RFE(estimator=ExtraTreeRegressor(), n_features_to_select=n_features, step=step)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### III.V. Wrap Linear SVR Regression
def RFE_linear_svr_regression(X, y):
    step = 1
    n_features = 5
    Xs_with_0s = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        transformer = RFE(estimator=LinearSVR(), n_features_to_select=n_features, step=step)
        X_new = transformer.fit_transform(X, y[colname])
        Xs_with_0s.append(transformer.inverse_transform(X_new))
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X(X, Xs_with_0s)
    if X_result.shape[1]==0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ## IV. RFECV from sklearn.feature_selection




# step(1), k-fold(5), and min_features_to_select(1) for all



# ### IV.I. Wrap Linear Regression
def RFECV_linear_regression(X, y):
    step = 1
    k_fold = 5
    n_features = 1
    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = RFECV(estimator=LinearRegression(), step=1, cv=5)
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))
        return None

# ### IV.II. Wrap Ridge Regression
def RFECV_ridge_regression(X, y):
    step = 1
    k_fold = 5
    n_features = 1
    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = RFECV(estimator=Ridge(), step=1, cv=5)
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))
        return None

# ### IV.III. Wrap Lasso Regression
def RFECV_lasso_regression(X, y):
    step = 1
    k_fold = 5
    n_features = 1
    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = RFECV(estimator=Lasso(), step=1, cv=5)
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))
        return None

# ### IV.IV. Wrap Extra Trees Regression
def RFECV_extra_trees_regression(X, y):
    step = 1
    k_fold = 5
    n_features = 1
    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = RFECV(estimator=ExtraTreeRegressor(), step=1, cv=5)
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))
        return None

# ### IV.V. Wrap Linear SVR Regression
def RFECV_linear_svr_regression(X, y):
    step = 1
    k_fold = 5
    n_features = 1
    try:
        Xs_with_0s = []
        time_init = time.time()
        for index in range(0,y.shape[1]):
            colname =  y.columns[index]
            transformer = RFECV(estimator=LinearSVR(), step=1, cv=5)
            X_new = transformer.fit_transform(X, y[colname])
            Xs_with_0s.append(transformer.inverse_transform(X_new))
        time_end = time.time()
        print("Time to select features:", (time_end-time_init))
        X_result = get_resulting_X(X, Xs_with_0s)
        if X_result.shape[1]==0:
            print("Zero features selected")
            return None
        score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

        return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}
    except Exception as e:
        print(colored('ERROR: '+str(e), 'red'))
        return None

# ## V. Feature Importances from yellowbrick.model_selection

# topn=15, stack=false,relative=true

# ### V.I. Wrap Linear Regression
def visual_FeatureImportances_linear(X,y):


    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        viz = FeatureImportances(LinearRegression())
        viz.fit(X, y[colname])
        #viz.show()
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))


# ### V.II. Wrap Ridge Regression
def visual_FeatureImportances_ridge(X,y):

    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        viz = FeatureImportances(Ridge())
        viz.fit(X, y[colname])
        #viz.show()
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))


# ### V.III. Wrap Lasso Regression
def visual_FeatureImportances_lasso(X,y):

    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        viz = FeatureImportances(Lasso())
        viz.fit(X, y[colname])
        #viz.show()
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))


# ### V.IV. Wrap Extra Trees Regression
def visual_FeatureImportances_extra_trees(X,y):

    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        viz = FeatureImportances(ExtraTreeRegressor())
        viz.fit(X, y[colname])
        #viz.show()
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))


# ### V.V. Wrap Linear SVR Regression
def visual_FeatureImportances_svr(X,y):

    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        viz = FeatureImportances(LinearSVR())
        viz.fit(X, y[colname])
        #viz.show()
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))


# ## VI. RFECV from yellowbrick.model_selection

# step=1, cv=5

# ### VI.I. Wrap Linear Regression
def yellowbrick_RFECV_linear_regression(X, y):
    rankings = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        viz = RFECV(LinearRegression())
        viz.fit(X, y[colname])
        rankings.append(viz.ranking_)
        #viz.show()
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X_yellowbrick(X, rankings)
    if X_result.shape[1] == 0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### VI.II. Wrap Ridge Regression
def yellowbrick_RFECV_ridge_regression(X, y):

    rankings = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        viz = RFECV(Ridge())
        viz.fit(X, y[colname])
        rankings.append(viz.ranking_)
        #viz.show()
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X_yellowbrick(X, rankings)
    if X_result.shape[1] == 0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### VI.III. Wrap Lasso Regression
def yellowbrick_RFECV_lasso_regression(X, y):

    rankings = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        viz = RFECV(Lasso())
        viz.fit(X, y[colname])
        rankings.append(viz.ranking_)
        #viz.show()
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X_yellowbrick(X, rankings)
    if X_result.shape[1] == 0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### VI.IV. Wrap Extra Tree Regression
def yellowbrick_RFECV_extra_trees_regression(X, y):

    rankings = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        viz = RFECV(ExtraTreeRegressor())
        viz.fit(X, y[colname])
        rankings.append(viz.ranking_)
        #viz.show()
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X_yellowbrick(X, rankings)
    if X_result.shape[1] == 0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}


# ### VI.V. Wrap Linear SVR Regression
def yellowbrick_RFECV_linear_svr_regression(X, y):

    rankings = []
    time_init = time.time()
    for index in range(0,y.shape[1]):
        colname =  y.columns[index]
        viz = RFECV(LinearSVR())
        viz.fit(X, y[colname])
        rankings.append(viz.ranking_)
        #viz.show()
    time_end = time.time()
    print("Time to select features:", (time_end-time_init))
    X_result = get_resulting_X_yellowbrick(X, rankings)
    if X_result.shape[1] == 0:
        print("Zero features selected")
        return None
    score, mse, mae, _ , _ , _ = evaluate_subset(X_result,y)

    return {"r2_score": score, "mse": mse, "mae": mae, "number_of_original_features": X.shape[1], "number_of_selected_features":X_result.shape[1], "selected_features":list(X_result.columns)}

