import json
from joblib import dump
from zipfile import ZipFile
import os

from sklearn.model_selection  import train_test_split
import matplotlib.pyplot as plt
import time
from sklearn.multioutput import MultiOutputRegressor #for multiple output models
from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error

from sklearn.linear_model     import LinearRegression, Ridge, Lasso
from sklearn.tree import ExtraTreeRegressor
from sklearn.svm import LinearSVR

OUTPUT_FOLDER = "output_models"



def get_results_per_model(model, X_train, X_test, y_train, y_test):
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    score = r2_score(y_test, y_pred)
    mse = mean_squared_error(y_test, y_pred)
    mae = mean_absolute_error(y_test, y_pred)
    return score, mse, mae


def get_best_model(X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

    if y.shape[1] > 1:
        lr_model = MultiOutputRegressor(LinearRegression())
        ridge_model = MultiOutputRegressor(Ridge())
        lasso_model = MultiOutputRegressor(Lasso())
        extra_model = MultiOutputRegressor(ExtraTreeRegressor())
        svr_model = MultiOutputRegressor(LinearSVR())
    else:
        lr_model = LinearRegression()
        ridge_model = Ridge()
        lasso_model = Lasso()
        extra_model = ExtraTreeRegressor()
        svr_model = LinearSVR()

    lr_r2_score, lr_mse, lr_mae = get_results_per_model(lr_model, X_train, X_test, y_train, y_test)
    ridge_r2_score, ridge_mse, ridge_mae = get_results_per_model(ridge_model, X_train, X_test, y_train, y_test)
    lasso_r2_score, lasso_mse, lasso_mae = get_results_per_model(lasso_model, X_train, X_test, y_train, y_test)
    extra_r2_score, extra_mse, extra_mae = get_results_per_model(extra_model, X_train, X_test, y_train, y_test)
    svr_r2_score, svr_mse, svr_mae = get_results_per_model(svr_model, X_train, X_test, y_train, y_test)

    models_names = ["Linear Regression", "Ridge", "Lasso", "Extra Trees", "SVR"]
    models = [lr_model, ridge_model, lasso_model, extra_model, svr_model]
    r2_scores = [lr_r2_score, ridge_r2_score, lasso_r2_score, extra_r2_score, svr_r2_score]
    mse_values = [lr_mse, ridge_mse, lasso_mse, extra_mse, svr_mse]
    mae_values = [lr_mae, ridge_mae, lasso_mae, extra_mae, svr_mae]

    best_r2_score = max(r2_scores)
    bm_i = r2_scores.index(best_r2_score)

    best_model = models[bm_i]
    stats = {"model": models_names[bm_i], "r2_score": r2_scores[bm_i], "mse": mse_values[bm_i], "mae": mae_values[bm_i]}

    return best_model, stats


def generate_regression_model(X,y):

    return get_best_model(X,y)


def generate_classification_model(X,y):

    return model


def generate_model(type, X, y, token):

    print(X.columns)
    print(y.columns)

    if type == "regression":
        model, stats = generate_regression_model(X,y)
    elif type== "classification":
        model, stats = generate_classification_model(X,y)
    else:
        return None

    if stats == None:
        return None
    else:
        path = os.path.join(OUTPUT_FOLDER)
        try:
            os.stat(path)
        except:
            os.mkdir(path)

        path = os.path.join(OUTPUT_FOLDER, token)
        try:
            os.stat(path)
        except:
            os.mkdir(path)

        #stats
        with open(os.path.join(path,'stats.json'), 'w') as fp:
            json.dump(stats, fp)
            fp.close()

        #cabeceras inputs
        with open(os.path.join(path,'inputs.txt'), 'w') as fp:
            fp.write(",".join(list(X.columns)))
            fp.close()

        #cabeceras salida
        with open(os.path.join(path,'outputs.txt'), 'w') as fp:
            fp.write(",".join(list(y.columns)))
            fp.close()

        #dataset resultante
        dataset = X.join(y)
        dataset.to_csv(os.path.join(path,'dataset.csv'), index=False)

        #modelo
        dump(model, os.path.join(path,'model.joblib'))

        #zip all
        zipfilename = 'model.zip'
        zipfilepath= os.path.join(path, zipfilename)
        with ZipFile(zipfilepath, 'w') as zipObj:
            # Add multiple files to the zip
            zipObj.write(os.path.join(path,'stats.json'),'stats.json')
            zipObj.write(os.path.join(path,'inputs.txt'),'inputs.txt')
            zipObj.write(os.path.join(path,'outputs.txt'),'outputs.txt')
            zipObj.write(os.path.join(path,'dataset.csv'),'dataset.csv')
            zipObj.write(os.path.join(path,'model.joblib'),'model.joblib')
            zipObj.close()

    return path, zipfilename